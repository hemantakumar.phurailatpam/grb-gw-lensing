import matplotlib.pyplot as plt
import numpy as np
import os

base_dir = '/Users/amore/Documents/MML_PhilTrans/fig_1/PLpert_fig_data/'
config_data = {
    'FOLD': {'pairs': ['(1-2)', '(1-3)', '(1-4)', '(2-3)', '(2-4)', '(3-4)'], 'title': 'Fold Configuration'},
    'CROSS': {'pairs': ['(1-2)', '(1-3)', '(1-4)', '(2-3)', '(2-4)', '(3-4)'], 'title': 'Cross Configuration'},
    'CUSP': {'pairs': ['(1-2)', '(1-3)', '(1-4)', '(2-3)', '(2-4)', '(3-4)'], 'title': 'Cusp Configuration'},
    'DOUBLE': {'pairs': ['(1-2)'], 'title': 'Double Configuration'}  }
#######################################################################################
def load_data(directory, pairs):
    difference_ratios = []
    percentages = []

    for num in range(1, 4):
        lens_dir = os.path.join(directory, f'Lens{num}') ##/Run1')
        mockdata_file = os.path.join(lens_dir, 'mockdata_del_phi.dat')
        model_file = os.path.join(lens_dir, 'model_del_phi.dat')

        if os.path.exists(mockdata_file) and os.path.exists(model_file):
            mockdata = np.loadtxt(mockdata_file)
            model = np.loadtxt(model_file)

            difference_ratio = (mockdata - model) / mockdata
            difference_ratio = np.ravel(difference_ratio)
            difference_ratios.append(difference_ratio)

            percent_difference = difference_ratio * 100
            percentages.append(percent_difference)

    return difference_ratios, percentages

#######################################################################################
def plot_func(ax, difference_ratios, percentages, config):
    pairs = config['pairs']
    title = config['title']
    markers = ['o', 's', '*']
    colors = ['Navy', 'SkyBlue', 'Cyan']
    labels = ['Lens 1', 'Lens 2', 'Lens 3']

    for i, ratio in enumerate(difference_ratios):
        ax.scatter(range(len(pairs)), ratio, marker=markers[i], lw=1., label=labels[i], edgecolor='black', s=450, color=colors[i])

    ax.set_xticks(range(len(pairs)))
    ax.set_xticklabels(pairs, rotation=45, fontsize=12)
    ax.set_title(title)
    ax.set_xlabel('Pairs', fontsize=15)
    ax.set_ylabel(r'$\delta\Delta\Psi$', fontsize=15)
    ax.axhline(0, color='black', linestyle='--')
    ax.grid(True)

    ax_percent = ax.twinx()
    for i, percent in enumerate(percentages):

      ax_percent.set_ylabel('Uncertainty (%)', fontsize=15)
      ax_percent.set_ylim(ax.get_ylim()[0] * 100, ax.get_ylim()[1] * 100)  # Set twin axis y-limits to be 100 times main plot y-limits
#######################################################################################

fig, axs = plt.subplots(2, 2, figsize=(15, 10))

for idx, (config_name, config) in enumerate(config_data.items()):
    difference_ratios, percentages = load_data(os.path.join(base_dir, f'{config_name}_june'), config['pairs'])
    plot_func(axs[idx // 2, idx % 2], difference_ratios, percentages, config)

handles, labels = axs[0, 0].get_legend_handles_labels()
fig.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, 0), fontsize=20, ncol=6)

plt.tight_layout()
plt.savefig("del_psi.png", bbox_inches='tight')

